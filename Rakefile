namespace 'prod' do
  require 'droplet_kit'
  require './lib/cloud_config_generator.rb'
  REGION = 'nyc2'
  SIZE = '512mb'
  IMAGE = 'ubuntu-14-04-x64'
  DNS_DOMAIN = 'do.co6.org'

  @docean = DropletKit::Client.new(
    access_token: File.read('keys/private/do_id').chomp
  )

  def upload_ssh_key
    ssh_keys = @docean.ssh_keys
    if (pillar_key = ssh_keys.all.find { |x| x.name == 'pillar_console' })
      pillar_key.id
    else
      @docean.ssh_keys.create(
        DropletKit::SSHKey.new(
          name: 'pillar_console',
          public_key: File.read('keys/pillar_console.pub')
        )
      ).id
    end
  end

  def docean_ip_update(name, id)
    ip = @docean.droplets.find(id: id)['networks']['v4'][0]['ip_address']
    @docean.domain_records.create(
      DropletKit::DomainRecord.new(
        type: 'A',
        name: name,
        data: ip
      ),
      for_domain: DNS_DOMAIN
    )
  end

  def docean_deploy(server_class)
    @docean.droplets.create(
      DropletKit::Droplet.new(
        name:       "#{server_class}.do.co6.org",
        region:     REGION,
        size:       SIZE,
        image:      IMAGE,
        user_data:  gen_cloud_config(server_class),
        ssh_keys:   [upload_ssh_key]
      )
    )['id']
  end

  task :deploy_web do |t|
    id = docean_deploy('web')
    sleep 20
    docean_ip_update('web', id)
  end
  task :deploy_app do |t|
    id = docean_deploy('app')
    sleep 20
    docean_ip_update('app', id)
  end
  task :deploy_db do |t|
    id = docean_deploy('db')
    sleep 20
    docean_ip_update('db', id)
  end
  task deploy_all: %w(prod:deploy_db prod:deploy_app prod:deploy_web)
  task :decom do
    %w(web app db).each do |role|
      puts "Decom " + role
    end
  end
end
