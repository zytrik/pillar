require 'yaml'

CHEF_VERSION = '12.7.2'
# This becomes /root/.ssh/config and allows git to clone the deployment repo
# without user intervention
SSH_CONFIG = <<-EOS
Host bitbucket.org
  StrictHostKeyChecking no
  IdentityFile /root/.ssh/chefkey
EOS

# Return a yaml formatted string to pass to the cloud provider
def gen_cloud_config(node_class)
  chef_client_config = <<-EOS
cookbook_path "/srv/deploy/#{node_class}/cookbook/berks-cookbooks/"
role_path '/srv/deploy/#{node_class}/roles'
data_bag_path  '/srv/deploy/#{node_class}/data_bags'
environment_path '/srv/deploy/#{node_class}/environments'
encrypted_data_bag_secret '/etc/chef'
environment 'production'
local_mode 'true'
node_name '#{node_class}'
node_path "/srv/deploy/#{node_class}/nodes"
log_level :info
EOS

  "#cloud-config\n" + {
    'packages' => ['git'],
    'write_files' => [
      { 'path' => '/root/.ssh/chefkey',
        'permissions' => '0600',
        'content' => File.read('keys/pillar_deploy') },
      { 'path' => '/root/.ssh/config',
        'content' => SSH_CONFIG },
      { 'path' => '/etc/chef/client.rb',
        'content' => chef_client_config
      }
    ],
    'runcmd' => [
      ['mkdir', '--mode=0755', '/srv/deploy'],
      ['git', 'clone', 'git@bitbucket.org:zytrik/pillar.git', '/srv/deploy'],
      ['curl', '-L', '-o', '/tmp/install-chef.sh',
       'https://www.chef.io/chef/install.sh'],
      ['bash', '/tmp/install-chef.sh', '-v', CHEF_VERSION],
      ['chef-client'],
      ['chef-client'],
      ['chef-client', '-d']
    ]
  }.to_yaml
end
