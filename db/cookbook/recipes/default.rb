#
# Cookbook Name:: cookbook
# Recipe:: default
#
# Copyright (C) 2016 YOUR_NAME
#
# All rights reserved - Do Not Redistribute
#

apt_update 'apt_update' do
  action :update
end

package 'mongodb'

service 'mongodb' do
  action :start
  action :enable
end

cookbook_file '/etc/mongodb.conf' do
  source 'mongodb.conf'
  action :create
  notifies :restart, 'service[mongodb]', :immediate
end

bash 'mongodb_seed' do
  cwd '/srv/deploy/db/seed/'
  notifies :start, 'service[mongodb]', :before
  code <<-EOH
    set -e
    mkdir -p /srv/mongo || true
    mongo pillar seed.js
    touch /srv/mongo/.seeded
  EOH
  not_if { ::File.exist?('/srv/mongo/.seeded') }
end
