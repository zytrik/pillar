#
# Cookbook Name:: cookbook
# Recipe:: default
#
# Copyright (C) 2016 YOUR_NAME
#
# All rights reserved - Do Not Redistribute
#
apt_update 'apt_update' do
  action :update
end

package 'nginx'

service 'nginx' do
  action :start
end

link '/etc/nginx/sites-enabled/default' do
  action :delete
  notifies :restart, 'service[nginx]', :delayed
end

cookbook_file '/etc/nginx/sites-available/pillar' do
  source 'nginx-pillar'
  action :create
end

link '/etc/nginx/sites-enabled/pillar' do
  to '/etc/nginx/sites-available/pillar'
  action :create
  notifies :restart, 'service[nginx]', :delayed
end
