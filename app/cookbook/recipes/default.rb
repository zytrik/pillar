#
# Cookbook Name:: cookbook
# Recipe:: default
#
# Copyright (C) 2016 YOUR_NAME
#
# All rights reserved - Do Not Redistribute
#
apt_update 'apt_update' do
  action :update
end

package ['curl', 'gnupg', 'build-essential', 'git']

bash 'rvm_install' do
  cwd '/tmp'
  code <<-EOH
    gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
    curl -sSL https://get.rvm.io | bash -s stable --ruby
    /usr/local/rvm/wrappers/default/gem install bundle
    EOH
  not_if { ::File.exists?('/usr/local/rvm') }
end

user 'passenger' do
  home '/srv/passenger'
  manage_home true
  system true
end

# This is an ugly workaround because git works defferent in prod than in my
# vagrant instance.
bash 'copy_app_repo' do
  cwd '/srv/passenger'
  code <<-EOH
    cp -R /srv/deploy pillar_app
    chown -R passenger /srv/passenger/pillar_app
  EOH
  not_if { ::File.exists?('/srv/passenger/pillar_app') }
end

bash 'bundle_install' do
  cwd '/srv/passenger/pillar_app/app/pillar_app'
  user 'passenger'
  code <<-EOH
    /usr/local/rvm/wrappers/default/bundle install --deployment --without test development
  EOH
  not_if {
    ::File.exists?('/pillar_app/app/pillar_app/vendor/bundle')
  }
end

cookbook_file '/etc/init/passenger.conf' do
  source 'passenger.conf'
  action :create
end

service 'passenger' do
  action [:start, :enable]
end
