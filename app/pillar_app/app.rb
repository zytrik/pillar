require 'sinatra/base'
require 'mongoid'
require 'tilt/erb'

Mongoid.load!('mongoid.yml')

APP_SERVER_FINGERPRINT="app1"

class Thing
  include Mongoid::Document
  store_in collection: "thingList"
  field :name
end

class PillarApp < Sinatra::Base
  get '/' do
    erb :index, :locals => {
      :app => APP_SERVER_FINGERPRINT,
      :db => Thing.all.first.name
    }
  end
end
