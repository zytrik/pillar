# Pillar Automated Deployment README

This repository contains an automated Chef deployment of a 3 tier web
application utilizing nginx, a lightweight Sinatra app running on passenger, and MongoDB. The app is built to be deployed to DigitalOcean and tested on Vagrant,
but it should be easy to adapt for deployment with any cloud provider with Ruby
bindings.

This app is only a brief demo for pre-employment purposes, and is not production
ready.

## Requirements
- An installation of a modern version of Ruby
- Ruby bundler
- An account with DigitalOcean and it's corresponding API key.

## Quickstart
Generate the required files in keys/private and run the folling:

    $ bundle install
    $ rake prod:deploy_all

## What's Included
There are three main directories with core files used by Chef-client in support
of standalone mode:

    [web, app, db]/
      cookbook/
      roles/
      environments/
      nodes/
      data_bags/
      cookbook/
        berks-cookbooks/

  Files containing the data hosted by each respective server:

    web/static/
    app/pillar_app/
    db/seed/

`lib/` contains a generator for the cloud-init yaml used for deployment
bootstrapping.

`keys/` contains sensitive data like ssh keys used during deployment. Note:
Database passwords are stored in the chef data. It's not ideal, but sufficient
for this demo.

## What's Not Included

    keys/private/
      do_id
      pillar_console

These files should contain the API key for DigitalOcean and the private SSH key
paired with `pillar_deploy.pub`, respectively.

## Deployment Strategy

| Workstation | Cloud Provider | Cloud Instance |
|---------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------|
| Start deployment using rake |  |  |
| Generate user_data yaml with repo private key |  |  |
| Set up DNS, verify that the pillar_deploy public ssh key exists, and start the build. |  |  |
|  | Create a cloud instance, add SSH public key for root authentication, and present user_data yaml. |  |
|  |  | Clone this git repository to /srv/deploy using the included private SSH deployment key. |
|  |  | Run Chef Omnibus installer. |
|  |  | Start chef-client in local mode using the appropriate directory in /srv/deploy |

### Rationale
I've decided to use Chef-Client in local mode in order to minimize additional
infrastructure components, and create a very scalable ecosystem. Each server
could be broken out into individual repositories to reduce data leakage, and it
should be trivial to automate live project updates that don't require reloading
the cloud instances.

The tradeoff, however, is some gaps in monitoring which could be remidied with
much needed log aggregation and status tracking components.

## TODOs and Open Issues
- The app server really needs it's own repository so we can take advantage of
Chef's Capistrano-style deployment capabilities.
- Database keys are mixed in carelessly with the chef recipies.
- The deprovisioning pieces of the rake script still need built.
- In a real world cloud deployment we would have SSL encryption between each tier in the system. An x509 infrastructure seemed like excessive complexity for a tech demo.
- In hindsight, berkshelf is pretty unnecessary. It would have been helpful if I had pulled in supermarket recipes, but that turned out to be overkill for such a small system. Now we just make an superfluous copy of each recipe for no reason.
- Each tier has access to the other tier's configuration data. This was a deliberate choice to keep everything in a single repo, but is a bad idea in the real world.
- When chef is seeding the database, it fails the first run because of service startup delay. My quck-and-dirty workaround was to just run Chef a few times (thanks statefulness!) but it would be a good idea to get chef to wait to seed until the database is actually ready.